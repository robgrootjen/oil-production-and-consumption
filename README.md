The datapackage shows how much barrels per day are produced and consumed in each country + historical data worldwide since 1980

## Data
The data is sourced from: 
* Oil consumption worldwide per year - https://www.indexmundi.com/energy/
* Oil production per country - https://en.wikipedia.org/wiki/List_of_countries_by_oil_production
* Oil consumption per country per day https://en.wikipedia.org/wiki/List_of_countries_by_oil_consumption


The repo includes:
* datapackage.json
* oilconsumption.csv
* oilproduction.csv
* worldwide.csv
* process.py

--------------------------------------------------------------------------------------------------------------------------------

## Preparation
Requires:
1. Python
2. Visualstudio / Jupyter notebook / or any platform that works with python.

***oilconsumption.csv*** (Shows how many barrels are consumed per day.)
* The CSV Data has 2 colummns. 
    * Country/Region
    * Oil consumption(bbl/day)
    

***oilproduction.csv***  (Shows how many barrels are produced per day.)
* The CSV Data has 2 columns.
    * Country
    * Oil Production(bbl/day)


***worldwide.csv*** (Shows yearly oil consumption worldwide since 1980.)

`IMPORTANT` Scale is 1000 barrels per day (multiply every value in consumption column * 1000)

* The CSV Data has 2 columns.
    * year
    * consumption

***datapackage.json***
* The json file has all the information from the two csv files, licence, author and 2 line graphs for every table.

***process.py***
* This script will scrape 3 different tables from 3 different sources.
    * Oil consumption worldwide per year - https://www.indexmundi.com/energy/
    * Oil production per country - https://en.wikipedia.org/wiki/List_of_countries_by_oil_production
    * Oil consumption per country per day https://en.wikipedia.org/wiki/List_of_countries_by_oil_consumption
    
***Instructions:***
* Copy process.py script in the "Process" folder.
* Open in jupyter notebook, python shell, VS code, or any preferred platform.
* Run the code
* 3 CSV files will be saved in document where your terminal is at the moment.
----------------------------------------------------------------------------------------------------------------------------------------

## Licence
This Data Package is made available under the Public Domain Dedication and License v1.0 